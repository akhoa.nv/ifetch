#include <kipr/wombat.h>
int normalize(int max_normalize, int max_value, int value)
{
    return (value*max_normalize)/max_value;
}
int main()
{
    int stage = 0;
    
	create_connect();
  	camera_open(); // Opens camera
    enable_servos();
	set_servo_position(0, 530);
    
    while (1) {
      camera_update(); // Updates camera until it succeeds
      if (stage == 0) { // Find blue balls
          
          create_drive_direct(normalize(450, 159, get_object_center_x(0, 0)), normalize(450, 159, 159 - get_object_center_x(0, 0)));
          
          if (get_object_center_y(0, 0) > 100) {
              stage = 1;
              set_create_distance(0);
              create_stop();
          }
      } else if (stage == 1) { // backup initially
          if (get_create_distance() > -100) {
              create_drive_straight(-100);
          } else {
              stage = 2;
              set_create_total_angle(0);
              create_stop();
          }
      } else if (stage == 2) { // Spinning 180
      	  if (get_create_total_angle() < 210) {
          	create_spin_CCW(50);
          } else {
              stage = 3;
              set_create_distance(0);
              set_servo_position(0, 800);
              create_stop();
          }
      } else if (stage == 3) { // Backup
          if (analog(0) > 3350) {
              create_drive_straight(-70);
              printf("\n%d\n", get_create_distance());
          }
          if (analog(0) < 3350 || get_create_distance() < -500) {
              stage = 4;
              create_stop();
          }
      } else if (stage == 4) { // Close claws
          set_servo_position(0, 500);
          
		  stage = 5;
      } else if (stage == 5) { // Find box
          if(analog(0) > 3500) {
              stage = 0;
          } else {
          
           create_drive_direct(normalize(500, 159, get_object_center_x(1, 0)), normalize(500, 159, 159 - get_object_center_x(1, 0)));

              if (get_object_center_y(1, 0) > 50) {
                  create_stop();   
              }
          }
      }

        msleep(50);
    }
    camera_close();
   	disable_servos();
	create_disconnect();
    return 0;
}
